package fr.univtours.polytech.location.model;

import java.io.Serializable;
import java.util.Base64;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 * Entity implementation class for Entity: LocationBean
 *
 */
@Entity
@XmlRootElement //Si coudn't find MessageBodyWriter for application/xml
public class LocationBean implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Double nightPrice;
	private String address;
	private String city;
	private String zipCode;
	@Lob
	@Column(columnDefinition = "BLOB")
	private byte[] picture;
	private static final long serialVersionUID = 1L;

	public LocationBean() {
		super();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getNightPrice() {
		return this.nightPrice;
	}

	public void setNightPrice(Double nightPrice) {
		this.nightPrice = nightPrice;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@XmlTransient //Pour que ca ne s'affiche pas dans le json dans Postman
	public byte[] getPicture() {
		return this.picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	/**
	 * Transforme le byte[] contenant les donn�es de l'images en String.
	 * 
	 * @return La cha�ne de caract�re permettant l'affichage d'une image depuis une
	 *         JSP.
	 */
	@XmlTransient
	public String getBase64Image() {
		if (null != this.picture && !"".equals(this.picture)) {
			return Base64.getEncoder().encodeToString(this.picture);
		} else {
			return "";
		}
	}
}
