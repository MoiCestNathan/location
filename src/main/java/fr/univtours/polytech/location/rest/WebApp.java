package fr.univtours.polytech.location.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("api")
public class WebApp extends Application {
    
}