package fr.univtours.polytech.location.rest;

import java.util.List;

import fr.univtours.polytech.location.business.LocationBusinessLocal;
import fr.univtours.polytech.location.model.LocationBean;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("v1") // Regle 0 : Commencer par /v1 pour la version 1 de l'API REST
@Stateless
public class LocationRest {

    @EJB // @Inject c'est pareil
    private LocationBusinessLocal business;

    /**
     * V1 de la methode get toutes les locations
     * 
     * @param tri
     * @param filtre
     * @return
     */
    @Path("locations") // Regle 1 : au PLURIEL
    @GET // Regle 2 : Utiliser GET pour recuperer des donnees
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON }) // Regle 3 : Accepter les deux formats XML et
                                                                         // JSON
    public List<LocationBean> getLocations() {
        return business.getLocations();
        // GET http://localhost:8080/location/api/v1/locations
    }

    /**
     * Gerer le tri et le filtre pour selectionner toutes les locations
     * 
     * @param tri
     * @param filtre
     * @return
     */
    // @Path("locations") //Regle 1 : au PLURIEL
    // @GET //Regle 2 : Utiliser GET pour recuperer des donnees
    // @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON}) //Regle 3
    // : Accepter les deux formats XML et JSON
    // public List<LocationBean> getLocations(@QueryParam("tri") String
    // tri,@QueryParam("filtre") String filtre) {
    // if ("".equals(filtre) && "".equals(tri)){
    // return business.getLocations();
    // }
    // return null;
    // }

    @Path("locations/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
    public LocationBean getLocationById(@PathParam("id") int id) {
        return business.getLocation(id);
        // GET http://localhost:8080/location/api/v1/locations/{id}
    }

    @DELETE
    @Path("/{id}/delete")
    public Response deletePerson(@PathParam("id") int id,
            @HeaderParam(HttpHeaders.AUTHORIZATION) String authorizationHeader) {
        if (authorizationHeader == null) {
            return Response.status(Response.Status.UNAUTHORIZED).build(); // 401 Pas authorise
        }
        if (!"42".equals(authorizationHeader)) {
            return Response.status(Response.Status.FORBIDDEN).build(); // 403 Acces refuse
        }
        if (id != 3) {
            return Response.status(Response.Status.FORBIDDEN).build(); // 403 Acces refuse
        }
        if (business.getLocation(id) == null) {
            return Response.status(Response.Status.NOT_FOUND).build();// 404 Erreur ressource
        }
        business.deleteLocation(id);
        return Response.ok().build();// 200 Succes
        // DELETE http://localhost:8080/location/api/v1/3/delete
        // Ne pas oublier d'aller dans Headers et ajouter key : Authorization keep-alive : 42
    }

    // Méthode appelée lorsqu'on ajoute toutes les informations dans le corps de la
    // requête.
    @Path("locations")
    @POST
    @Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createLocation(LocationBean locationBean) {
        business.addLocation(locationBean);
        return Response.ok().build();
        //POST http://localhost:8080/location/api/v1/locations
    }

}
