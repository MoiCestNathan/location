package fr.univtours.polytech.location.dao;

import java.util.List;

import fr.univtours.polytech.location.model.LocationBean;

public interface LocationDao {

	public void createLocation(LocationBean bean);

	public List<LocationBean> getLocations();

	public LocationBean getLocation(Integer id);

	public void updateLocation(LocationBean locationBean);

	public void deleteLocation(LocationBean locationBean);
}
